# NativeScript-Vue Application

> A native todo application built with NativeScript-Vue. It uses an expressJS backend API and saves the data for each user in the cloud. THe idea is that the same todos could be seen from a web inteface or a desktop app using the same database,

## Usage

> If you have properly configured vue-nativescript environment

``` bash
# Install dependencies
npm install

# Build for production
npm run build
npm run build:<platform>

# Build, watch for changes and debug the application
npm run debug
npm run debug:<platform>

# Build, watch for changes and run the application
npm run watch
npm run watch:<platform>

# Clean the NativeScript application instance (i.e. rm -rf dist)
npm run clean
```