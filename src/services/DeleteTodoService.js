import Api from './Api'

export default {
  deleteTodo (token, id) {
    return Api().delete(`/todos/${id}`, {
      headers: {
        'x-auth': token
      }
    })
  }
}