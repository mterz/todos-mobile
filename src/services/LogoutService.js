import Api from './Api'

export default {
  logout (token) {
    return Api().delete('/users/me/token', {
      headers: {
        'x-auth': token
      }
    })
  }
}