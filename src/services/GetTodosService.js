import Api from './Api'

export default {
  fetch (token) {
    return Api().get('/todos', {
      headers: {
        'x-auth': token
      }
    })
  }
}