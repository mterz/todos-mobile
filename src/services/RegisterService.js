import Api from './Api'

export default {
  register (email, password) {
    return Api().post('/users', {
      email,
      password
    })
  }
}