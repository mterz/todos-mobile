import Api from './Api'

export default {
  login (email, password) {
    return Api().post('/users/login', {
      email,
      password
    })
  }
}