import axios from 'axios'

export default () => {
  return axios.create({
    baseURL: 'https://lit-coast-42960.herokuapp.com/'
  })
}