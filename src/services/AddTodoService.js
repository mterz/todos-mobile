import Api from './Api'

export default {
  addTodo (token, text, completed) {
    return Api().post('/todos', {
      text,
      completed
    }, {
      headers: {
        'x-auth': token
      }
    })
  }
}